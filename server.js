const http = require("http")
const fs = require("fs")
const qs = require("querystring")

function sendFile(fileName, res) {
    let directory = "static"
    if (fileName == "/") fileName = "/index.html"
    if (fileName.includes("/libs")) directory = __dirname
    fs.readFile(`${directory}${fileName}`, function (error, data) {
        if (error) {
            console.log(error)
            res.writeHead(404, { "Content-Type": "text/html;charset=utf-8" })
            res.end(`Błąd 404: Nie znaleziono pliku ./static${fileName}`)
        } else {
            let ext = fileName.split(".")[1]
            let contentType = "text/html;charset=utf-8"
            switch (ext) {
                case "js":
                    contentType = "application/javascript"
                    break
                case "css":
                    contentType = "text/css"
                    break
                case "jpg":
                    contentType = "image/jpeg"
                    break
                case "mp3":
                    contentType = "audio/mpeg"
                    break
                case "png":
                    contentType = "image/png"
                    break
                case "svg":
                    contentType = "image/svg+xml"
                    break
                default: break
            }
            res.writeHead(200, { "Content-Type": contentType })
            res.end(data)
        }
    })
}

var mediaRoot = "/static/media"
var dirsAndFiles
var selectedDir

function scanMediaDirs(res) {
    dirsAndFiles = {
        dirs: [],
        files: [],
        currentDir: ""
    }
    fs.readdir(__dirname + mediaRoot, function (err, files) {
        if (err) {
            return console.log(err);
        }
        files.forEach(function (dirName) {
            //console.log(dirName);
            dirsAndFiles["dirs"].push(dirName)
        })

        selectedDir = selectedDir || dirsAndFiles["dirs"][0]
        dirsAndFiles.currentDir = selectedDir
        fs.readdir(`${__dirname}${mediaRoot}/${selectedDir}`, function (error, dirFiles) {
            if (error) {
                return console.log(error);
            }

            dirFiles.forEach(function (fileName) {
                if (fileName.substr(-4) != ".mp3") return
                let fileStats = fs.statSync(__dirname + mediaRoot + "/" + selectedDir + "/" + fileName)
                let fileSize = (Math.round(fileStats.size / 1000000.00 * 100) / 100.00) + "  MB"

                dirsAndFiles["files"].push({ name: fileName, size: fileSize })
            })

            res.writeHead(200, { "Content-Type": "application/json" })
            res.end(JSON.stringify(dirsAndFiles, null, 4))
        })
    })
}

function servResponse(req, res) {
    let allData = ""
    req.on("data", function (data) {
        allData += data
    })
    req.on("end", function (data) {
        console.log("Data(" + req.url + "):" + allData)
        switch (req.url) {
            case "/LOAD":
                let obj = qs.parse(allData)
                selectedDir = obj.dirName
                scanMediaDirs(res)
                break
            default:
                console.log("Błędny POST: " + req.url)

                res.writeHead(400, { "Content-Type": "application/json;charset=utf-8" })
                res.end("Błędna akcja POST: " + req.url)
                break
        }
    })
}

var server = http.createServer(function (req, res) {
    console.log(req.method + " : " + req.url)
    switch (req.method) {
        case "GET":
            sendFile(decodeURI(req.url), res)
            break;
        case "POST":
            servResponse(req, res)
            break;
    }
})

const port = 3000
server.listen(3000, function () {
    console.log("Start serwera na porcie " + port)
})