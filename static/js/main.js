var net, ui, music, visual
$(document).ready(function () {
    net = new Net()
    ui = new Ui()
    music = new Music()
    visual = new Visual()
    window.addEventListener("keyup", function (event) {
        if (event.code == "Space")
            visual.toggle()
        else if (event.code == "ArrowLeft")
            $("#btn_previous").click()
        else if (event.code == "ArrowRight")
            $("#btn_next").click()
        else if (event.code == "CapsLock") {
            if ($("#audio").prop("paused"))
                $("#audio").trigger("play")
            else
                $("#audio").trigger("pause")
        }
    })
    net.sendData()
})