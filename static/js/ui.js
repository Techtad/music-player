class Ui {
    constructor() {
        this.selectedRow = null
        this.rowButton = null
        this.selectedRepeatIndex = 0
        this.clicks()
    }

    clicks() {
        $("#btn_play_pause").on("click", function (event) {
            if (ui.selectedRow == null) {
                let firstSongBtn = $(document).find(".selection-button").first()
                music.selectSong(firstSongBtn.attr("song"), firstSongBtn.attr("title"))
            }

            if ($("#audio").prop("paused"))
                $("#audio").trigger("play")
            else
                $("#audio").trigger("pause")
        })
        $("#btn_next").on("click", function (event) {
            if (ui.selectedRow == null) {
                let firstSongBtn = $(document).find("[index=0]").first().find(".selection-button").first()
                music.selectSong(firstSongBtn.attr("song"), firstSongBtn.attr("title"))
            } else {
                if (music.onPlaylist) {
                    let index = parseInt(ui.selectedRow.attr("index"))
                    index = ++index % music.playlist.length
                    let nextRow = $(`#playlist-${index}`)
                    let nextSongBtn = nextRow.find(".list-play-button").first()
                    music.selectSongOnPlaylist(nextSongBtn.attr("index"))
                } else {
                    let index = parseInt(ui.selectedRow.attr("index"))
                    index = ++index % (ui.selectedRow.siblings().length + 1)
                    let nextRow = $(document).find(`[index=${index}]`).first()
                    let nextSongBtn = nextRow.find(".selection-button").first()
                    music.selectSong(nextSongBtn.attr("song"), nextSongBtn.attr("title"))
                }
            }
        })
        $("#btn_previous").on("click", function (event) {
            if (ui.selectedRow == null) {
                let firstSongBtn = $(document).find("[index=0]").first().find(".selection-button").first()
                music.selectSong(firstSongBtn.attr("song"), firstSongBtn.attr("title"))
            } else {
                if (music.onPlaylist) {
                    let index = parseInt(ui.selectedRow.attr("index"))
                    index = --index % music.playlist.length
                    if (index == -1) index = music.playlist.length - 1
                    let nextRow = $(`#playlist-${index}`)
                    let nextSongBtn = nextRow.find(".list-play-button").first()
                    music.selectSongOnPlaylist(nextSongBtn.attr("index"))
                } else {
                    let index = parseInt(ui.selectedRow.attr("index"))
                    index = --index % (ui.selectedRow.siblings().length + 1)
                    if (index == -1) index = ui.selectedRow.siblings().length
                    let nextRow = $(document).find(`[index=${index}]`).first()
                    let nextSongBtn = nextRow.find(".selection-button").first()
                    music.selectSong(nextSongBtn.attr("song"), nextSongBtn.attr("title"))
                }
            }
        })
        $("#progress-bar").on("click", function (event) {
            if (ui.selectedRow == null) return;
            $("#audio").prop("currentTime", ((event.pageX - $("#progress-bar").offset().left) / $("#progress-bar").width()) * $("#audio").prop("duration"))
        })
    }

    reload(data) {
        $("#covers").empty()
        $("#songs").empty()

        $("#title").text(data.currentDir)

        for (let dir of data["dirs"]) {
            let li = $('<li>')
            let img = $('<img>')
            img.attr("src", `media/${dir}/cover.jpg`)
            img.attr("alt", dir)
            li.append(img)
            $("#covers").append(li)
        }
        let index = 0
        for (let file of data["files"]) {
            let row = $('<tr>')
            row.attr("id", `tr-${music.getSongTitle(file.name).replace(/\W/g, "_")}`)
            row.attr("index", index++)
            row.append(`<td>${data.currentDir}`)
            row.append(`<td>${music.getSongTitle(file.name)}`)
            row.append(`<td>${file.size}`)

            let buttonsCell = $("<td>")
            let selectButton = $("<img>")
            selectButton.attr("src", "img/play.svg")
            selectButton.attr("song", `media/${data.currentDir}/${file.name}`)
            selectButton.attr("title", music.getSongTitle(file.name))
            selectButton.addClass("row-button")
            selectButton.addClass("selection-button")
            let addButton = $("<img>")
            addButton.attr("src", "img/add.svg")
            addButton.attr("album", data.currentDir)
            addButton.attr("song", `media/${data.currentDir}/${file.name}`)
            addButton.attr("title", music.getSongTitle(file.name))
            addButton.addClass("row-button")
            addButton.addClass("add-button")
            buttonsCell.append(selectButton);
            buttonsCell.append(addButton);
            row.append(buttonsCell);

            $("#songs").append(row)

            if (!music.onPlaylist && music.selectedSong == addButton.attr("song")) {
                ui.selectedRow = row
                ui.selectedRow.addClass("selected-row")
                ui.rowButton = ui.selectedRow.find(".selection-button")
                ui.rowButton.attr("src", $("#btn_play_pause").attr("src"))
            }
        }

        $("#covers").children().each(function () {
            $(this).on("click", function (event) {
                net.sendData($(event.target).attr("alt"))
            })
        })

        $(".selection-button").each(function () {
            $(this).on("click", function (event) {
                if (!ui.selectedRow || ui.selectedRow.attr("id") != this.parentElement.parentElement.id) {
                    music.selectSong($(event.target).attr("song"), $(event.target).attr("title"))
                    $("#audio").trigger("play")
                } else {
                    if ($("#audio").prop("paused"))
                        $("#audio").trigger("play")
                    else
                        $("#audio").trigger("pause")
                }
            })
        })

        $(".add-button").each(function () {
            $(this).on("click", function (event) {
                music.addToPlaylist(this.attr("album"), this.attr("title"), this.attr("song"))
                ui.displayPlaylist()
            }.bind($(this)))
        })
    }

    updateTime() {
        let progressMinutes = Math.floor($("#audio").prop("currentTime") / 60.0) || 0
        let progressSeconds = Math.floor($("#audio").prop("currentTime") - progressMinutes * 60.0) || 0
        let durationMinutes = Math.floor($("#audio").prop("duration") / 60.0) || 0
        let durationSeconds = Math.floor($("#audio").prop("duration") - durationMinutes * 60.0) || 0
        let twoDigits = function (number) { return number < 10 ? "0" + number : number }
        $("#progress-text").text(twoDigits(progressMinutes) + ":" + twoDigits(progressSeconds))
        $("#duration-text").text(twoDigits(durationMinutes) + ":" + twoDigits(durationSeconds))

        $("#progress").css("width", `${$("#audio").prop("currentTime") / $("#audio").prop("duration") * $("#progress-bar").width()}px`)
    }

    displayPlaylist() {
        $("#playlist").find("tr:not(:first-child)").remove()
        let playlist = music.getPlaylist()
        for (let track of playlist) {
            let row = $("<tr>")
            row.attr("id", `playlist-${track.index}`)
            row.attr("index", track.index)

            row.append($("<td>").text(track.index + 1))
            row.append($("<td>").text(track.album))
            row.append($("<td>").text(track.title))

            let playButton = $("<img>")
            playButton.attr("src", "img/play.svg")
            playButton.attr("index", track.index)
            playButton.addClass("row-button")
            playButton.addClass("list-play-button")

            let removeButton = $("<img>")
            removeButton.attr("src", "img/remove.svg")
            removeButton.attr("index", track.index)
            removeButton.addClass("row-button")
            removeButton.addClass("remove-button")

            row.append($("<td>").append(playButton).append(removeButton))

            $("#playlist").append(row)

            if (music.onPlaylist && music.selectedSong == track.song && ui.selectedRepeatIndex == track.repeatIndex) {
                ui.selectedRow = row
                ui.selectedRow.addClass("selected-row")
                ui.rowButton = ui.selectedRow.find(".list-play-button")
                ui.rowButton.attr("src", $("#btn_play_pause").attr("src"))
            }
        }

        $(".remove-button").each(function () {
            $(this).on("click", function (event) {
                if (!ui.selectedRow || ui.selectedRow.attr("id") != this.parent().parent().attr("id")) {
                    let track = music.playlist[parseInt(this.attr("index"))]
                    if (music.onPlaylist && music.selectedSong == track.song && ui.selectedRepeatIndex > track.repeatIndex) {
                        ui.selectedRepeatIndex--
                    }
                    music.removeFromPlaylist(this.attr("index"))
                    ui.displayPlaylist()
                }
            }.bind($(this)))
        })
        $(".list-play-button").each(function () {
            $(this).on("click", function (event) {
                let idx = this.attr("index")
                let track = music.getPlaylist()[idx]
                if (!ui.selectedRow || ui.selectedRow.attr("id") != this.parent().parent().attr("id")) {
                    music.selectSongOnPlaylist(idx)
                    $("#audio").trigger("play")
                } else {
                    if ($("#audio").prop("paused"))
                        $("#audio").trigger("play")
                    else
                        $("#audio").trigger("pause")
                }
            }.bind($(this)))
        })
    }
}