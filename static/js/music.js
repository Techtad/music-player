class Music {
    constructor() {
        this.bindEvents()
        this.playlist = []
        this.onPlaylist = false
        this.selectedSong = ""

        window.AudioContext = window.AudioContext || window.webkitAudioContext
        this.audioContext = new AudioContext()
        let audioElement = document.getElementById("audio")
        let source = this.audioContext.createMediaElementSource(audioElement)
        this.analyser = this.audioContext.createAnalyser()
        source.connect(this.analyser)
        this.analyser.connect(this.audioContext.destination)
        this.analyser.fftSize = 256
        this.dataArray = new Uint8Array(this.analyser.frequencyBinCount)
    }

    getAnalyserData() {
        this.analyser.getByteFrequencyData(this.dataArray)
        return this.dataArray
    }

    bindEvents() {
        $("#audio").bind("loadeddata", function () {
            $("#audio").trigger("play")
        })

        $("#audio").bind("play", function () {
            $("#btn_play_pause").attr("src", "img/pause.svg")
            ui.rowButton.attr("src", "img/pause.svg")
        })

        $("#audio").bind("pause", function () {
            $("#btn_play_pause").attr("src", "img/play.svg")
            ui.rowButton.attr("src", "img/play.svg")
        })

        $("#audio").bind("timeupdate", ui.updateTime)

        $("#audio").bind("ended", function () {
            $("#btn_next").click()
        })
    }

    selectSong(song, title) {
        this.selectedSong = song

        $("#audio").trigger("stop")
        $("#audio_src").attr("src", song)
        $("#audio").trigger("load")
        $("#selected-name").text(title)

        if (ui.selectedRow != null)
            ui.selectedRow.removeClass("selected-row")

        ui.selectedRow = $(`#tr-${title.replace(/\W/g, "_")}`)
        ui.selectedRow.addClass("selected-row")

        if (ui != null && ui.rowButton != null)
            ui.rowButton.attr("src", "img/play.svg")

        ui.rowButton = ui.selectedRow.find(".selection-button")

        this.onPlaylist = false
        ui.selectedRepeatIndex = 0
    }

    selectSongOnPlaylist(index) {
        this.selectedSong = this.playlist[index].song

        $("#audio").trigger("stop")
        $("#audio_src").attr("src", this.playlist[index].song)
        $("#audio").trigger("load")
        $("#selected-name").text(this.playlist[index].title)

        if (ui.selectedRow != null)
            ui.selectedRow.removeClass("selected-row")

        ui.selectedRow = $(`#playlist-${index}`)
        ui.selectedRow.addClass("selected-row")

        if (ui != null && ui.rowButton != null)
            ui.rowButton.attr("src", "img/play.svg")

        ui.rowButton = ui.selectedRow.find(".list-play-button")

        this.onPlaylist = true
        ui.selectedRepeatIndex = this.playlist[index].repeatIndex
    }

    getSongTitle(fileName) {
        return fileName.split(".")[0]
    }

    getPlaylist() {
        return this.playlist
    }

    addToPlaylist(album, title, song) {
        let repeatIndex = 0
        for (let track of this.playlist) {
            if (track.song == song) repeatIndex++
        }
        this.playlist.push({ index: this.playlist.length, album: album, title: title, song: song, repeatIndex: repeatIndex })
    }

    removeFromPlaylist(index) {
        let newPlaylist = []
        for (let track of this.playlist) {
            if (track.index != index) {
                let repeatIndex = 0
                for (let t of newPlaylist) {
                    if (track.song == t.song) repeatIndex++
                }
                track.index = newPlaylist.length
                track.repeatIndex = repeatIndex
                newPlaylist.push(track)
            }
        }
        this.playlist = newPlaylist
    }
}