class Circle {
    constructor(x, y, r) {
        this.x = x
        this.y = y
        this.radius = r
        this.element = $("<div>")
        this.element.addClass("circle")
        this.element.css("left", `${this.x - this.radius}px`)
        this.element.css("top", `${this.y - this.radius}px`)
        this.element.css("width", `${this.radius * 2}px`)
        this.element.css("height", `${this.radius * 2}px`)
    }

    circumference() {
        return 2 * Math.PI * this.radius
    }

    move(newX, newY) {
        this.x = newX
        this.y = newY
        this.element.css("left", `${this.x - this.radius}px`)
        this.element.css("top", `${this.y - this.radius}px`)
    }

    resize(newRadius) {
        this.radius = newRadius
        this.element.css("left", `${this.x - this.radius}px`)
        this.element.css("top", `${this.y - this.radius}px`)
        this.element.css("width", `${this.radius * 2}px`)
        this.element.css("height", `${this.radius * 2}px`)
    }
}

class Visual {
    constructor() {
        this.paused = true

        this.circle = new Circle(window.innerWidth / 2, window.innerHeight / 2, 160)
        $("#visualizer").append(this.circle.element)

        this.barCount = music.analyser.frequencyBinCount * 7 / 8

        for (let i = 0; i < this.barCount; i++) {
            let bar = $("<div>")
            bar.addClass("freq")
            bar.css("width", this.circle.circumference() / this.barCount + "px")

            $("#visualizer").append(bar)
        }
    }

    render() {
        let data = music.getAnalyserData()
        let sum = 0
        for (let i = 0; i < this.barCount / 2; i++) { sum += data[i] }
        this.circle.resize(sum / this.barCount / 255 * ($(window).height() * 0.61803398875 - 50) /* * 2 */ + 50)
        this.circle.move($(window).width() / 2, $(window).height() / 2)
        //this.circle.element.css("opacity", sum / music.analyser.frequencyBinCount / 255)
        for (let i = 0; i < this.barCount; i++) {
            let bar = $("#visualizer .freq").eq(i)

            let fData = i < this.barCount / 2 ? data[i] : data[this.barCount - i - 1]
            let length = fData / 255 * this.circle.radius * 0.61803398875 /* / 2 */
            bar.css("height", `${length}px`)
            bar.css("width", this.circle.circumference() / this.barCount + "px")

            let angleOnCircle = Math.PI * 2 / this.barCount * i
            //angleOnCircle += Math.PI

            bar.css("left", `${this.circle.x + (this.circle.radius * Math.sin(angleOnCircle)) - this.circle.circumference() / this.barCount / 2}px`)
            let yA = this.circle.y + (this.circle.radius * Math.cos(angleOnCircle))
            bar.css("top", `${yA}px`)

            let yB = this.circle.y + ((this.circle.radius + length) * Math.cos(angleOnCircle))
            let rotationAngle = Math.PI * 3 / 2 + Math.asin((yB - yA) / length)
            if (angleOnCircle > Math.PI)//if (angleOnCircle < Math.PI * 2)
                rotationAngle = -rotationAngle

            bar.css("transform", `rotate(${rotationAngle}rad)`)
        }

        if (!this.paused)
            requestAnimationFrame(this.render.bind(this))
    }

    pause() {
        this.paused = true
    }

    resume() {
        music.audioContext.resume().then(function () { console.log("audioContext lives!") })
        this.paused = false
        this.render()
    }

    show() {
        $("#visualizer").css("display", "block")
        this.resume()
    }

    hide() {
        $("#visualizer").css("display", "none")
        this.pause()
    }

    toggle() {
        if ($("#visualizer").css("display") == "none")
            this.show()
        else
            this.hide()
    }
}